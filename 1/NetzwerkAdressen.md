## Übersicht Adressen im Netzwerk

### MAC-Adresse

Die MAC-Adresse (Media Access Control) ist eine eindeutige Kennung, die jedem Netzwerkgerät, sei es ein Computer, Router oder Smartphone, zugeordnet ist. Sie besteht aus einer 48-Bit-Zahl, üblicherweise in sechs Gruppen von je zwei Hexadezimalziffern dargestellt und durch Doppelpunkte oder Bindestriche getrennt. Diese Adresse wird auf der Netzwerkebene verwendet, um sicherzustellen, dass Datenpakete an das richtige Gerät gesendet werden.

### IP-Adresse

Die IP-Adresse (Internet Protocol) ist eine numerische Kennung, die jedem Gerät in einem Netzwerk zugewiesen ist. Es gibt zwei Hauptversionen von IP-Adressen: IPv4 und IPv6. IPv4-Adressen bestehen aus vier Dezimalzahlen, die durch Punkte getrennt sind, während IPv6-Adressen längere Hexadezimalzahlen verwenden. IP-Adressen ermöglichen die eindeutige Identifizierung und Kommunikation von Geräten in einem Netzwerk.

### Host-Name / Gerätebezeichnung

Der Host-Name ist der Name, der einem Gerät in einem Netzwerk zugewiesen ist. Er wird häufig zur Identifizierung von Computern in einem lokalen Netzwerk verwendet. Der Host-Name ist menschenlesbar und bietet eine einfachere Möglichkeit, auf Geräte zuzugreifen, als dies durch die Verwendung von IP-Adressen allein der Fall wäre.

### DNS-Name

Der DNS-Name (Domain Name System) ist der Name eines Geräts oder einer Ressource im Internet, der über das DNS aufgelöst werden kann. DNS übersetzt menschenlesbare Domainnamen (wie www.example.com) in IP-Adressen, die für die Netzwerkkommunikation verwendet werden. Der DNS-Name ermöglicht es, Ressourcen im Internet leichter zu identifizieren und anzusprechen.
