# Agenda - 14.11.2023

- **Begrüßung / Moduleinstieg**

- **Besprechung der Modulidentifikation von ICT Berufsbildung Schweiz**

    Um die richtigen Infos zu finden gehe auf diesen Link: [Modulidentifikation](https://modulbaukasten.ch/Module/129_3_LAN-Komponenten%20in%20Betrieb%20nehmen.pdf)

- **Übersicht Adressen im Netzwerk**

    - MAC-Adresse
    - IP-Adresse
    - Host-Name / Gerätebezeichnung
    - DNS-Name

- **Übung mit Filius**

    1. [Filius Download](https://www.lernsoftware-filius.de/Herunterladen)
    2. Durchführung der Übungen 1-4 mit Filius

- **Übung "be a Switch"**

- **Hausaufgabe**

    - Training binärer Zahlen: [Cisco Learning Game](https://learningcontent.cisco.com/games/binary/index.html)
    - Abschluss der Filius-Übungen 1-4 mit Dokumentation

---

**Hinweis:** Dieses Dokument dient als Übersicht für die Agenda des 14.11.2023. Bitte aktualisiere es entsprechend den Fortschritten und Anforderungen während der Sitzung.

# Reflexion - 14.11.2023

Der heutige Tag war äußerst lehrreich und hat eine breite Palette von Themen im Bereich Netzwerktechnologien abgedeckt. Die folgende Reflexion fasst meine Eindrücke und Erkenntnisse zusammen:

## Modulidentifikation und Ressourcen

Die Diskussion über die Modulidentifikation von ICT Berufsbildung Schweiz hat mir einen klaren Überblick darüber verschafft, welche Inhalte wir im Rahmen dieses Moduls behandeln werden. Die zur Verfügung gestellten Ressourcen, insbesondere der Modulbaukasten, bieten eine strukturierte Grundlage für das Verständnis von LAN-Komponenten.

## Adressen im Netzwerk

Die Übersicht über verschiedene Arten von Adressen im Netzwerk, von MAC-Adressen bis zu DNS-Namen, hat meine Kenntnisse über die Grundlagen der Netzwerkkommunikation erweitert. Die Bedeutung jeder Adresse und ihre Rolle bei der Identifizierung von Geräten im Netzwerk wurden klar herausgestellt.

## Praktische Übungen mit Filius und "be a Switch"

Die praktischen Übungen mit Filius haben mir die Möglichkeit gegeben, mein theoretisches Wissen in die Praxis umzusetzen. Die Simulation von Netzwerkszenarien mit Filius und die Übung "be a Switch" haben meine Fähigkeiten im Umgang mit Netzwerkkomponenten verbessert.
