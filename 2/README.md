# Agenda - 21.11.2023

- **Offene Fragen klären**

- **Repetitionsquiz Routing**

- **Aufbau der IP-Adresse**

- **Zusammenhang IP-Adresse und Subnetzmaske**

    Tools / Hilfsmittel:
    
    - IP-Calc-BasicM129-v4-1.xlsx
    - IPCalculator.zip


- **Übung IP-Adresse / Subnetzmaske - Basics**


- **Alternative Erklärung zum Thema Subnetting**

    Bereitstellung einer alternativen Erklärung zum Thema Subnetting: [IPv4 Subnetting: Schritt für Schritt erklärt](https://www.itslot.de/2019/02/ipv4-subnetting-berechnen-schritt-fur.html)

- **Individuelles Lernen**

    - Vertiefung des Themas Subnetting
    - Vertiefung des Themas Routing anhand des Repetitionsquiz und der Präsentation "Netze mit Router verbindenV2.ppsx"
    - Aufbau eines vermaschten Netzwerks mit Filius

---

# Reflexion - Agenda 2 - 21.11.2023

Der heutige Tag war erneut von intensivem Lernen und praktischen Anwendungen geprägt. Hier sind meine Reflexionen zu den verschiedenen Aktivitäten:

## Repetitionsquiz Routing

Das Repetitionsquiz zum Thema Routing war eine wirksame Methode, um sicherzustellen, dass die grundlegenden Konzepte des Routings gut verstanden wurden. Die interaktive Natur des Quiz förderte eine aktive Beteiligung, wodurch das Wissen vertieft wurde.

## Aufbau der IP-Adresse

Die vertiefte Betrachtung des Aufbaus von IP-Adressen half dabei, ein tieferes Verständnis für die Struktur und die verschiedenen Teile der Adresse zu entwickeln. Dies ist von entscheidender Bedeutung für die spätere Anwendung von Subnetting.

## Zusammenhang IP-Adresse und Subnetzmaske

Die Erklärung des Zusammenhangs zwischen IP-Adressen und Subnetzmasken war besonders aufschlussreich. Die vorgestellten Tools, IP-Calc-BasicM129-v4-1.xlsx und IPCalculator.zip, erleichterten die praktische Umsetzung und förderten das Verständnis dieses wichtigen Konzepts.

## Übung IP-Adresse / Subnetzmaske - Basics

Die praktische Übung ermöglichte es, das erlernte Wissen direkt anzuwenden. Durch die Bearbeitung von Aufgaben im Zusammenhang mit IP-Adressen und Subnetzmasken konnten wir unsere Fähigkeiten weiterentwickeln und festigen.

## Alternative Erklärung zum Thema Subnetting

Die Bereitstellung einer alternativen Erklärung zum Thema Subnetting bot eine zusätzliche Perspektive und förderte ein umfassenderes Verständnis. Die Möglichkeit, verschiedene Ressourcen zu konsultieren, unterstützte uns dabei, das Konzept in seiner Tiefe zu erfassen.

## Individuelles Lernen

Die Phase des individuellen Lernens ermöglichte es jedem Teilnehmenden, auf seine individuellen Bedürfnisse einzugehen. Die vorgeschlagenen Vertiefungen zu Subnetting, Routing und dem Aufbau eines vermaschten Netzwerks mit Filius boten eine klare Struktur für das selbstgesteuerte Lernen.

Insgesamt war der Tag äußerst produktiv, und die Mischung aus Theorie, praktischen Übungen und individuellem Lernen trug maßgeblich dazu bei, ein solides Verständnis der Netzwerkthemen zu entwickeln. Ich freue mich darauf, das erworbene Wissen in den kommenden Tagen weiter zu vertiefen.
