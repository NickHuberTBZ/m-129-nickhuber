## Der Strukturierte Aufbau von IP-Adressen

IP-Adressen sind die Bausteine des modernen Netzwerks, die es ermöglichen, Geräte miteinander zu identifizieren und zu verbinden. Der Aufbau von IP-Adressen folgt bestimmten Regeln, die eine klare Zuordnung und effiziente Datenübertragung gewährleisten. Hier werfen wir einen genaueren Blick auf den strukturierten Aufbau von IP-Adressen:

### IPv4 vs. IPv6

Es existieren zwei Hauptversionen von IP-Adressen: IPv4 (Internet Protocol Version 4) und IPv6 (Internet Protocol Version 6). IPv4-Adressen bestehen aus 32 Bits, während IPv6-Adressen 128 Bits umfassen. Diese Adressen können unterschiedlich dargestellt werden, wobei IPv4 oft in Dezimalzahlen und IPv6 in Hexadezimalzahlen angegeben wird.

### Der Aufbau einer IPv4-Adresse

Eine IPv4-Adresse setzt sich aus vier Oktetten (Bytes) mit je 8 Bits zusammen. Die Oktetten sind durch Punkte getrennt und repräsentieren verschiedene Teile der Adresse. Jedes Oktett kann einen Wert zwischen 0 und 255 annehmen. Zum Beispiel steht die IPv4-Adresse "192.168.0.1" für vier Oktetten, wobei jedes einen numerischen Wert darstellt.

### Beispiel einer IPv4-Adresse:

- Oktett 1: 192
- Oktett 2: 168
- Oktett 3: 0
- Oktett 4: 1

![BilderIPv4](IPv4-Calc.png)

### Der Aufbau einer IPv6-Adresse

IPv6-Adressen sind komplexer und länger als IPv4-Adressen. Sie bestehen aus acht Blöcken mit je 16 Bits und werden durch Doppelpunkte getrennt. Jeder Block repräsentiert einen Abschnitt der Adresse. Eine Beispiel-IPv6-Adresse lautet "2001:0db8:85a3:0000:0000:8a2e:0370:7334".

### Beispiel einer IPv6-Adresse:

- Block 1: 2001
- Block 2: 0db8
- Block 3: 85a3
- Block 4: 0000
- Block 5: 0000
- Block 6: 8a2e
- Block 7: 0370
- Block 8: 7334

### Klassenlose Adressierung und Subnetting

Moderne Netzwerke setzen oft auf klassenlose Adressierung und Subnetting, um IP-Adressen effizienter zu nutzen. Subnetting erlaubt die Aufteilung eines Netzwerks in kleinere, logische Subnetze, was zu einer optimierten Ressourcennutzung führt.

Der strukturierte Aufbau von IP-Adressen bildet das Grundgerüst des Internets, und ein fundiertes Verständnis dieser Struktur ist entscheidend für die effektive Konfiguration und Verwaltung von Netzwerken.
