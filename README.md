# Nick Huber - Modul 129

Willkommen in meinem GitLab-Repository zum Thema Netzwerk! Dieses Repository dient dazu, meine persönlichen Notizen und Fortschritte von dem Modul 129 aufzeichnen.

## Projektarbeit

Alle Inhalte meiner Projektarbeit finden sie [hier!](Arbeit-Cisco)

## Inhalte

- **Tag-1 14.11.2023:** [Repetition M117](/1)
- **Tag-2 21.11.2023:** [Aufbau von eines Netzwerks](/2)
- **Tag-3 28.11.2023:** [Repetition Tag 1 und Tag 2](/3)
- **Tag-4 05.12.2023:** [Switches und Netzwerk Befehle](/4)
- **Tag-5 12.12.2023:** *Arbeiten an Tag 1-4*
- **Tag-6 19.12.2023:** *Arbeiten an Tag 1-4*
- **Tag-6 09.01.2024:** *Prüfung und Vorbeitung Fehlersuche im Netzwerk*
- **Tag-7 16.01.2024:** *Entwicklung für Prüfung und Start der Arbeit*
