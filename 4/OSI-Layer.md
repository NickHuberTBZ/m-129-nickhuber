# OSI-Modell

Das OSI-Modell (Open Systems Interconnection) ist ein konzeptionelles Framework, das dazu dient, Netzwerkkommunikation zu verstehen. Es ist in sieben Schichten unterteilt, von denen jede für spezifische Funktionen im Kommunikationsprozess verantwortlich ist.

![](OSI-Modell.png)

## 1. Physikalische Schicht

- **Funktion:** Definiert das physische Medium, wie Kabel und Steckverbinder.
- **Geräte:** Hubs, Repeater.

## 2. Sicherungsschicht

- **Funktion:** Bietet Fehlererkennung und -korrektur.
- **Geräte:** Switches, Bridges.

## 3. Netzwerkschicht

- **Funktion:** Verwaltet logische Adressierung und Routing.
- **Geräte:** Router, Layer-3-Switches.

## 4. Transportschicht

- **Funktion:** Gewährleistet die Ende-zu-Ende-Kommunikation, Zuverlässigkeit und Fehlerwiederherstellung.
- **Protokolle:** TCP (Transmission Control Protocol), UDP (User Datagram Protocol).

## 5. Sitzungsschicht

- **Funktion:** Stellt Verbindungen (Sitzungen) zwischen Anwendungen her, wartet sie und beendet sie.
- **Protokolle:** NetBIOS, RPC (Remote Procedure Call).

## 6. Darstellungsschicht

- **Funktion:** Übersetzt Daten zwischen der Anwendungsschicht und den unteren Schichten.
- **Aufgaben:** Verschlüsselung, Komprimierung.

## 7. Anwendungsschicht

- **Funktion:** Bietet eine Netzwerkschnittstelle für Endbenutzeranwendungen.
- **Protokolle:** HTTP, FTP, SMTP.

