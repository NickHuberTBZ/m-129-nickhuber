# Agenda 4 vom 05.12.2023

### Teil 1 

**Arbeitsblatt "Fehlersuche im Netzwerk.pdf"** (siehe Files - Fehlersuche im Netzwerk.pdf) bearbeiten
- **Ersatzbefehle für Power-Shell austesten** (Beispiel cmdlet Test-Connection) 
- Für Linux-Befehle virtuelles Linux verwenden
    - ubuntu auf windows emulieren als Subsystem
       - [Tutorial Download Ubuntu Subsystem](https://ubuntu.com/tutorials/install-ubuntu-on-wsl2-on-windows-10#1-overview)
       - oder [Slax Linux](https://www.slax.org/) - winziges Livesystemen
    - Download live Puppy Linux (https://puppylinux.com): [Iso Datei puppyLinux](http://distro.ibiblio.org/puppylinux/puppy-fossa/fossapup64-9.5.iso)
- Repetition OSI-Layer und TCP-Layer 
    - [Netzwerkschichten-mitSymbolen.pdf](Netzwerkschichten-mitSymbolen.pdf)
    - Repetitionsquiz Assignments: Card - access it on https://go.skype.com/cards.unsupported

---

### Teil 2
- **Switch-Arten kennenlernen** - Übersicht erstellen, unterschiedliche Switches mit den Eigenschaften vergleichen
    - Ressourcen nutzen [Herdt Netzwerkgrundlagen](Herdt_Netzwerke_Grundlagen_2022) 13.4 S. 139-142
- Beispiel Ressource Forum:
    - https://community.fs.com/de/blog/ethernet-switch-port-types-overview.html  
- Beispiel Ressource Hersteller:
    - https://www.netgear.com/support/product/S3300-52X-PoE%242b%2b%2428GS752TXP%2429#docs  
    - https://www.downloads.netgear.com/files/GDC/S3300/S3300_SWA_EN.pdf  
- Switchfunktionalität kennen lernen
    - Tools BeASwitch
    - Übung in Packetracer https://www.packettracernetwork.com/labs/lab1-basicswitchsetup.html 
    - Weitere Ressource:
        - Netzwerksicherheit | Layer 2 Security | CAM Table Attack https://www.nanoo.tv/link/v/NBeabMWn
        - virtuelle Switches
        - HyperV  
            - https://learn.microsoft.com/en-us/windows-server/virtualization/hyper-v-virtual-switch/hyper-v-virtual-switch
            - https://www.altaro.com/hyper-v/the-hyper-v-virtual-switch-explained-part-1/
        - AWS  
            - https://docs.aws.amazon.com/directconnect/latest/UserGuide/WorkingWithVirtualInterfaces.html
            - https://aws.amazon.com/de/blogs/compute/selecting-network-switches-for-your-aws-outposts/
