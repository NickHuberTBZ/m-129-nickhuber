# Testen des Netz 

Ich habe nur 2 Tests durchgeführt aber diese bedeuten schon viel. Einfach vom Netz 1 habe ich gepingt, auf Netz 2 und 3. Dadurch sehe ich ob ich richtig durch die Router komme und die Routing-Tabelle richtig geschrieben ist.

## Test 1
Ping von Client-7 *192.168.1.11* auf Client-4 *192.168.3.11*

![Test-1](Test-1.png)

## Test 2
Ping von Client-7 *192.168.1.11* auf Client-1 *192.168.2.10*

![Test-2](Test-2.png)
