# Betriebsdokumentation - Projekt M129

![Titelbild](Titelbild-M129.png)

## Ziel der Arbeit

Die Subnetzaufteilung in drei verschiedene Netzwerke ermöglicht eine gezielte Segmentierung des Datenverkehrs, was zu einer verbesserten Sicherheit und effizienteren Netzwerkverwaltung führt. Die Implementierung eines DHCP-Servers in Netz 1 erleichtert die automatische Zuweisung von IP-Adressen an Clients, was zu einer effizienten IP-Adressenverwaltung und vereinfachten Konfiguration der Endgeräte beiträgt. Die Einrichtung von Routern und die Konfiguration der Routing-Tabellen ermöglichen einen geordneten Datenfluss zwischen den Subnetzen, wodurch eine praktische Anwendung von Routingkonzepten und eine Vermeidung von Endlosschleifen gewährleistet sind.

## Infrastruktur

### Ports

Da uns bei einem Netzwerk dieser Grösse ein 1gb/s ausreichen verwenden wir **`1CFE`** (**C**ooper, **F**ast, **E**thernet) für die Router, Switches und Endgeräte. Mit den 1CFE kann man ausserdem auch Kosten sparen, da dies nicht so eine teure Version ist. Zusätzlich kann man mit dieser Version der Ports in Zukunft auf VLan umsteigen wenn man es möchte.

### Switches

Bei den Switches habe ich die Version 2960 IOS15 verwendet. Ich habe die Version des Switches genommen, weil man diese selbstständig konfigureren und verwalten muss.

### Kabel

Um finanzielle Ressourcen zu schonen, erfolgt die Verwendung von Kupferkabeln für die gesamte Infrastruktur. Allerdings wird für die Verbindung von Router zu Router ein sogenanntes **Copper Crossover** Kabel eingesetzt, während innerhalb der Netzwerke **Copper Straight** verwendet wird.

Dies resultiert daraus, dass Copper Straight optimal für die Verbindung zwischen verschiedenen Geräten geeignet ist, während Copper Crossover vorwiegend für die Verbindung gleicher Geräte genutzt wird.

Trotz der heutzutage weit verbreiteten Möglichkeit, dies in den meisten Netzwerkkarten umzustellen und ohne erkennbaren Unterschied zu betreiben, aber auf Cisco Packet Tracer muss man immernoch Cooper Crossover verwenden.

### Router

Bei den Routern haben wir **PT-Empty** verwendet, um sie von Grund auf konfigurieren zu können. Dadurch ermöglichen wir die bestmögliche Lernentwicklung.

### Switches

Bei den Switches habe ich die Version **2960 IOS15** verwendet. Ich habe die Version des Switches genommen, weil man diese selbstständig konfigureren und verwalten muss.


## Netzwerketopologie

![Netzwerktopologie](Netzwerktopologie/Netzwerktopologie-Bild.png)

### Netzwerkgeräte

- Router - 3
- Switch - 3
- Clients - 7
- Server - 1

## Netzwerke

### Netz 1

In diesem Netz habe ich einen DHCP Server aufgesetzt für die Aufteilung der IP-Adressen. Dies habe ich gemacht um zu sehen wie es ist einen DHCP Server aufzusetzen und zu verwenden.

 - Subnetmaske - *255.255.255.0*
 - Server-1 - *192.168.1.5* 
    - Funktion: Dieser Server ist zu einem DHCP Server konfiguriert, damit alle Hosts in diesem Subnetz eine Adresse zugeteilt bekommen. 
    - DHCP Range: *192.168.1.10* - *192.168.1.255*
    - Default Gateway: *192.168.1.1*
 - Client-6 - *192.168.1.10* "zugeteilt von DHCP"
 - Client-7 - *192.168.1.11* "zugeteilt von DHCP"
 

### Netz 2

Das Netzwerk 2 wurde von Grund auf statisch aufgesetzt, wobei die folgenden Konfigurationselemente benutzt wurden:

 - Subnetmaske - 255.255.255.0
 - Default Gateway: *192.168.2.1*
 - Client-1 - *192.168.2.10*
 - Client-2 - *192.168.2.11*
 - Client-3 - *192.168.2.12*


### Netz 3

Netzwerk 3 weist eine identische Struktur auf, jedoch mit unterschiedlichen Netzwerkadressen, da sie in einem anderen Netz stationiert sind.

 - Subnetmaske - 255.255.255.0
 - Default Gateway: *192.168.3.1*
 - Client-4 - *192.168.3.10*
 - Client-5 - *192.168.3.11*


## Routing-Tabelle

### Router-1

|Network/destination|Subnetmask|Next Hop|TTL|
|-----|-----|-----|-----|
|192.168.2.0|255.255.255.0|172.16.1.2|4|
|192.168.3.0|255.255.255.0|172.16.1.2|4|


### Router-2

|Network/destination|Mask|Next Hop|TTL|
|-----|-----|-----|-----|
|192.168.1.0|255.255.255.0|172.16.1.1|4|
|192.168.3.0|255.255.255.0|172.16.2.2|4|


### Router-3

|Network/destination|Mask|Next Hop|TTL|
|-----|-----|-----|-----|
|192.168.1.0|255.255.255.0|172.16.2.1|4|
|192.168.2.0|255.255.255.0|172.16.2.1|4|

## Netzwerk Infrastruktur

![NetzwerkInfrastruktur](Cisco-Packet-Tracer/Screenshot-Cisco-M129.png)
