Ich hatte Mühe mit den Oktetten mit dem Rechnen zu einer Dezimalzahl.

Um von der Dezimalzahl `134'976'524` zur Darstellung in Oktetten (IPv4-Adressformat) zu gelangen, zerlegen wir die Zahl in die entsprechenden Potenzen von 256, die für jedes Oktett verwendet werden.

Gegebene Dezimalzahl: `134'976'524`

1. **Oktett 1 (von links):** `134'976'524 / 256^3 = 8`
2. **Oktett 2:** `(134'976'524 / 256^2) mod 256 = 45`
3. **Oktett 3:** `(134'976'524 / 256^1) mod 256 = 39`
4. **Oktett 4 (von rechts):** `134'976'524 mod 256 = 100`

Daher ergeben sich die Oktetten für die Dezimalzahl `134'976'524` als `8.45.39.100`.
