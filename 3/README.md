# Agenda - 28.11.2023

- **Individuelles Lernen**

  Die Sitzung beginnt mit einer Phase des individuellen Lernens, in der Teilnehmende die Möglichkeit haben, sich auf spezifische Themen zu konzentrieren und ihre persönlichen Lernziele zu verfolgen.

- **Repetition Subnetting**

  Eine Wiederholung des Themas Subnetting bietet die Gelegenheit, bereits erworbenes Wissen zu festigen und eventuelle Unsicherheiten zu klären. Die Teilnehmenden werden ermutigt, Fragen zu stellen und aktiv am Wiederholungsprozess teilzunehmen.

-   **Bestehende Übungen (Teams-Aufgaben)**

    Teilnehmende setzen ihre Zusammenarbeit in Teams fort, um bestehende Aufgaben im Zusammenhang mit Subnetting zu bearbeiten und zu vertiefen.

- **Repetition Routing mit Filius**

    - Bestehende Übungen (Teams-Aufgaben) und eigene Ergänzungen

- **Weiterführendes Programm**

    - Einführung in Cisco Packet Tracer (netacad)

    - Registrierung bei netacad mit edu.tbz-E-Mailadresse
  [Netacad Registrierung](https://www.netacad.com/)

    - Download Cisco Packet Tracer Version 8.1.1 English
  [Packet Tracer Download](https://www.netacad.com/portal/resources/packet-tracer)

    - Einführung:
  [Skillsforall](https://skillsforall.com/topics/cisco-packet-tracer?utm_source=packet-tracer&utm_medium=app&utm_campaign=packet-tracer-app-link)
  [PacketTracerNetwork](https://www.packettracernetwork.com/)

    - Tutorials:
  [Tutorials PTNetAcad](http://tutorials.ptnetacad.net/tutorials80.htm)

    - Labs (Laboraufgaben mit Anleitung, vergleichbar mit Filius-Aufgaben aber näher an der Realität):
  [PacketTracerLabs](https://www.packettracernetwork.com/labs/packettracerlabs.html)

  **Empfohlene Reihenfolge:**
  1. Lab 1
  2. Lab 2
  3. Lab 6
  4. Lab 7
  5. Lab 4
  6. Lab 5

  Tipp für Übersetzungen EN - DE:
  [DeepL Translator](https://www.deepl.com/translator) oder [AWS-Kurse gemäß CCG [Networking].pdf](AWS-Kurse)

    - **GNS3 - Laborumgebung**

    Abschließend wird eine Einführung in die Laborumgebung GNS3 gegeben.

    [GNS3 - Laborumgebung](https://gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3)
